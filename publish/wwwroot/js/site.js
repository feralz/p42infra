﻿// This is an example of Leaflet usage; you should modify this for your needs.
var mymap = L.map('mapid', { zoomControl: false }).setView([45.750000, 4.850000], 12);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Génépi based on map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYWdvdWJzIiwiYSI6ImNrcHpqOG45NDAwZGsyb3JtYTQ4cGRqc3YifQ.l3alAAQuPyWvXl-DMBDyew'
}).addTo(mymap);

let Coords = {};

var optionsDepart = {
    position: 'topleft',
    expanded: true,
    placeholder: 'Point de départ',
    title: 'Point de départ',
    overrideBbox: true
};


var optionsStop = {
    position: 'topleft',
    expanded: true,
    placeholder: 'Point de destination',
    title: 'Point de destination'
};


var geocoderControl = new L.control.geocoder('pk.baa715e306bf4f0c58fd0a0f6621e1f9', optionsDepart).addTo(mymap).on('select', function (e) {
    Coords.startLat = e.latlng.lat;
    Coords.startLong = e.latlng.lng;
});

var geocoderControl2 = new L.control.geocoder('pk.baa715e306bf4f0c58fd0a0f6621e1f9', optionsStop).addTo(mymap).on('select', function (e) {
    Coords.stopLat = e.latlng.lat;
    Coords.stopLong = e.latlng.lng;
    Coords.TransportType = "all";
    console.log(Coords);

    broker.emit(Coords);
});


function carroute() {
    //sendAjax("api/carroute");
}

function cyclingroute() {
    //sendAjax("api/cyclingroute");
}

function footroute() {
    //sendAjax("api/footroute");
}

function sendAjax(url) {
    $.ajax({
        type: "POST",
        data: JSON.stringify(Coords),
        url: url,
        contentType: "application/json",
        Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
    }).done(function (data) {
        $(".leaflet-zoom-animated g .leaflet-interactive").remove();
        const parsedData = jQuery.parseJSON(data);
        const coords = parsedData.features[0].bbox;
        const extraData = {
            metrics: ["distance", "duration"],
            locations: [[coords[0], coords[1]], [coords[2], coords[3]]]
        };
        

        $.ajax({
            url: 'https://api.openrouteservice.org/v2/matrix/driving-car?api_key=5b3ce3597851110001cf62489a727653f4854a4f9c2f0bff539eae3c',
            type: 'POST',
            data: JSON.stringify(extraData),
            contentType: "application/json",    
            Accept: "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8"
        }).done(function (otherData) {
            console.log(otherData)
            $("#informations").html("Durée : " + Math.round(otherData.durations[1][0] / 60) + "min<br/>");
            $("#informations").append(getDistance(otherData.distances[1][0]));
            // ARNAUD TODO
        }).fail(function (e) { console.log(e); });

        console.log(extraData);
        L.geoJSON(parsedData).addTo(mymap);
    }).fail(function (e) { console.log(e); });
}

$(".leaflet-locationiq-close").on("click", function () {
    window.$(".leaflet-zoom-animated g .leaflet-interactive").remove();
});

function getDistance(distance) {
    if (distance < 1000) {
        return "Distance : " + distance + "m";
    } else {
        return "Distance : " + (distance/1000).toFixed(1) + "km";
    }
}